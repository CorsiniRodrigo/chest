<?php

namespace App\Controller;

use App\Entity\Video;
use App\Entity\Comentario;
use App\Form\VideoType;
use App\Form\ComentarioType;
use App\Repository\VideoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/video")
 */
class VideoController extends AbstractController
{
    /**
     * @Route("/", name="video_index", methods="GET")
     */
    public function index(VideoRepository $videoRepository): Response
    {     
        if(isset($_GET['search'])){
            return $this->render('video/index.html.twig', ['videos' => $videoRepository->findBy(
            ['titulo' => $_GET['search']])]);
        }
        return $this->render('video/index.html.twig', ['videos' => $videoRepository->findAll()]);
    
    }

    /**
     * @Route("/new", name="video_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {   

        $video = new Video();
        $form = $this->createForm(VideoType::class, $video);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($video);
            $em->flush();

            return $this->redirectToRoute('video_index');
        }

        return $this->render('video/new.html.twig', [
            'video' => $video,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="video_show",  methods="GET|POST")
     */
    public function show(Video $video,Request $request): Response
    {
        
        $em = $this->getDoctrine()->getManager();
        $comentarioRepository = $em->getRepository(Comentario::class);
        $comentarios = $comentarioRepository->findAll();
        $comentario = new Comentario();
        $formComentario = $this->createForm(ComentarioType::class, $comentario);
        $formComentario->handleRequest($request);

        if ($formComentario->isSubmitted() && $formComentario->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($comentario);
            $em->flush();
            $formComentario = $this->createForm(ComentarioType::class, $comentario);
            return $this->render('video/show.html.twig', ['video' => $video,
            'form' => $formComentario->createView(),'comentarios'=>$comentarios]);
        }

        return $this->render('video/show.html.twig', ['video' => $video,
            'form' => $formComentario->createView(),'comentarios'=>$comentarios]);




    }

    /**
     * @Route("/{id}/edit", name="video_edit", methods="GET|POST")
     */
    public function edit(Request $request, Video $video): Response
    {
        $form = $this->createForm(VideoType::class, $video);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('video_index', ['id' => $video->getId()]);
        }

        return $this->render('video/edit.html.twig', [
            'video' => $video,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="video_delete", methods="DELETE")
     */
    public function delete(Request $request, Video $video): Response
    {
        if ($this->isCsrfTokenValid('delete'.$video->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($video);
            $em->flush();
        }

        return $this->redirectToRoute('video_index');
    }

    /**
     * @Route("/buscar", name="video_buscar",methods="GET|POST")
     */
    public function buscarVideo(Request $request){
        dump();exit;
       /* $em = $this->getDoctrine()->getManager();
        $videoRepository = $em->getRepository(Video::class);
        $videos = $videoRepository->findBy(
            ['titulo' => $titulo],*/
        return $this->render('video/index.html.twig', ['videos' => $videos]);
            


    }

}
