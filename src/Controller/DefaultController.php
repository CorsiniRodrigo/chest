<?php

namespace App\Controller;

use App\Entity\Video;
use App\Form\VideoType;
use App\Repository\VideoRepository;
use App\Entity\Usuario;
use App\Form\UsuarioType;
use App\Repository\UsuarioRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/default", name="default")
     */
    public function index()
    {
       $em = $this->getDoctrine()->getManager();
        $videoRepository = $em->getRepository(Video::class);
         $usuarioRepository = $em->getRepository(Usuario::class);
        return $this->render('usuario/principal.html.twig', ['videos' => $videoRepository->findAll(),'usuarios' => $usuarioRepository->findAll()]);
    }
}
