<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181201114302 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE canal (id INT AUTO_INCREMENT NOT NULL, administrador_id INT DEFAULT NULL, nombre VARCHAR(255) DEFAULT NULL, subscriptores INT DEFAULT NULL, INDEX IDX_E181FB5948DFEBB7 (administrador_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE canal_usuario (canal_id INT NOT NULL, usuario_id INT NOT NULL, INDEX IDX_BA65FEFC68DB5B2E (canal_id), INDEX IDX_BA65FEFCDB38439E (usuario_id), PRIMARY KEY(canal_id, usuario_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE usuario (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(255) DEFAULT NULL, nombre VARCHAR(255) DEFAULT NULL, correo VARCHAR(255) DEFAULT NULL, role VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE video (id INT AUTO_INCREMENT NOT NULL, usuario_id INT DEFAULT NULL, canal_id INT DEFAULT NULL, titulo VARCHAR(255) DEFAULT NULL, duracion DATETIME DEFAULT NULL, descripcion VARCHAR(255) DEFAULT NULL, fecha DATETIME DEFAULT NULL, INDEX IDX_7CC7DA2CDB38439E (usuario_id), INDEX IDX_7CC7DA2C68DB5B2E (canal_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE canal ADD CONSTRAINT FK_E181FB5948DFEBB7 FOREIGN KEY (administrador_id) REFERENCES usuario (id)');
        $this->addSql('ALTER TABLE canal_usuario ADD CONSTRAINT FK_BA65FEFC68DB5B2E FOREIGN KEY (canal_id) REFERENCES canal (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE canal_usuario ADD CONSTRAINT FK_BA65FEFCDB38439E FOREIGN KEY (usuario_id) REFERENCES usuario (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE video ADD CONSTRAINT FK_7CC7DA2CDB38439E FOREIGN KEY (usuario_id) REFERENCES usuario (id)');
        $this->addSql('ALTER TABLE video ADD CONSTRAINT FK_7CC7DA2C68DB5B2E FOREIGN KEY (canal_id) REFERENCES canal (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE canal_usuario DROP FOREIGN KEY FK_BA65FEFC68DB5B2E');
        $this->addSql('ALTER TABLE video DROP FOREIGN KEY FK_7CC7DA2C68DB5B2E');
        $this->addSql('ALTER TABLE canal DROP FOREIGN KEY FK_E181FB5948DFEBB7');
        $this->addSql('ALTER TABLE canal_usuario DROP FOREIGN KEY FK_BA65FEFCDB38439E');
        $this->addSql('ALTER TABLE video DROP FOREIGN KEY FK_7CC7DA2CDB38439E');
        $this->addSql('DROP TABLE canal');
        $this->addSql('DROP TABLE canal_usuario');
        $this->addSql('DROP TABLE usuario');
        $this->addSql('DROP TABLE video');
    }
}
