<?php

namespace App\Form;

use App\Entity\Comentario;
use App\Entity\Video;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ComentarioType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('fecha', HiddenType::class, array(
                 //'data' => 'abcdef',
               // ))
            ->add('contenido',null, array(
                'label' => false,
                'attr' => array('placeholder' => 'Su comentario aquí')))
            ->add('video',EntityType::class, array(
                'class' => Video::class,
                'choice_label' => 'id',
                'label' => false,
));
            //->add('usuario')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Comentario::class,
        ]);
    }
}
